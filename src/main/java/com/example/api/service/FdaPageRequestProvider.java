package com.example.api.service;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class FdaPageRequestProvider {

    private final int pageSize;
    private final int pageNumber;

    public int getLimit() {
        return pageSize;
    }

    public int getSkip() {
        return pageSize * (pageNumber - 1);
    }
}
