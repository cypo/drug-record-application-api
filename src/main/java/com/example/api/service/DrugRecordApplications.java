package com.example.api.service;

import com.example.api.model.dto.DrugRecordApplicationDto;
import com.example.api.model.DrugRecordRequest;
import org.springframework.data.domain.Page;

public interface DrugRecordApplications {
    Page<DrugRecordApplicationDto> getPage(DrugRecordRequest request);
}
