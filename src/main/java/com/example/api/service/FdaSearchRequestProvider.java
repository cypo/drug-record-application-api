package com.example.api.service;

import com.example.api.model.DrugRecordRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class FdaSearchRequestProvider {
    private static final String MANUFACTURER_NAME_PARAM = "openfda.manufacturer_name:";
    private static final String FDA_BRAND_NAME_PARAM = "openfda.brand_name:";

    public static List<String> get(DrugRecordRequest request) {
        List<String> webParams = new ArrayList<>();

        Optional.ofNullable(request.getFdaManufacturerName())
                .map(manufacturerName -> MANUFACTURER_NAME_PARAM + manufacturerName)
                .ifPresent(webParams::add);
        Optional.ofNullable(request.getOptionalFdaBrandName())
                .map(brandName -> FDA_BRAND_NAME_PARAM + brandName)
                .ifPresent(webParams::add);
        return webParams;
    }
}
