package com.example.api.service;

import com.example.api.model.dto.ApplicationDetailsDto;
import com.example.api.model.entity.DrugDetailsEntity;
import com.example.api.model.mapper.ApplicationDetailsMapper;
import com.example.api.model.mapper.ProductNumberMapper;
import com.example.api.repository.DrugApplicationDetailsRepository;
import com.example.api.repository.ProductNumberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.example.api.model.mapper.ApplicationDetailsMapper.toEntity;

@Service
@RequiredArgsConstructor
public class DrugRecordApplicationDetailsService implements DrugRecordApplicationDetails {

    private final DrugApplicationDetailsRepository repository;
    private final ProductNumberRepository productNumberRepository;

    @Override
    @Transactional
    public String save(ApplicationDetailsDto dto) {
        DrugDetailsEntity entity = repository.save(toEntity(dto));
        productNumberRepository.saveAll(ProductNumberMapper.toEntity(dto, entity));
        return entity.getNumber();
    }

    @Override
    @Transactional
    public List<ApplicationDetailsDto> read() {
        return repository.findAll().stream()
                .map(ApplicationDetailsMapper::toDto)
                .toList();
    }
}
