package com.example.api.service;

import com.example.api.model.dto.ApplicationDetailsDto;

import java.util.List;

public interface DrugRecordApplicationDetails {
    String save(ApplicationDetailsDto applicationDetailsDto);
    List<ApplicationDetailsDto> read();
}
