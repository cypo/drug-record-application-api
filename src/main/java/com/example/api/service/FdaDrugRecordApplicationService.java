package com.example.api.service;

import com.example.api.exception.NoResultsFoundException;
import com.example.api.model.dto.DrugRecordApplicationDto;
import com.example.api.model.DrugRecordRequest;
import com.example.api.model.fda.FdaResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class FdaDrugRecordApplicationService implements DrugRecordApplications {
    private final WebClient webClient;

    public FdaDrugRecordApplicationService(@Value("${fda.url}") String fdaUrl) {
        this.webClient = WebClient.create(fdaUrl);
    }

    @Override
    public Page<DrugRecordApplicationDto> getPage(DrugRecordRequest request) {
        FdaPageRequestProvider pagingProvider = new FdaPageRequestProvider(request.getPageSize(), request.getPageNumber());
        FdaResponse fdaResponse = webClient.get()
                .uri(uriBuilder -> uriBuilder
                        .queryParam("search", FdaSearchRequestProvider.get(request))
                        .queryParam("limit", pagingProvider.getLimit())
                        .queryParam("skip", pagingProvider.getSkip())
                        .build())
                .retrieve()
                .onStatus(status -> status.value() == 404, error -> Mono.error(new NoResultsFoundException()))
                .bodyToMono(FdaResponse.class)
                .block();

        return toPage(request, fdaResponse);
    }

    private Page<DrugRecordApplicationDto> toPage(DrugRecordRequest request, FdaResponse fdaResponse) {
        return Optional.ofNullable(fdaResponse)
                .map(response -> new PageImpl<>(toDto(response), PageRequest.of(request.getPageNumber(), request.getPageSize()),
                        getTotal(response)))
                .orElse(new PageImpl<>(Collections.emptyList()));
    }

    private static long getTotal( FdaResponse response) {
        if (response.meta() == null || response.meta().results() == null) {
            return 0L;
        }
        return response.meta().results().total();
    }

    private List<DrugRecordApplicationDto> toDto( FdaResponse fdaResponse) {
        return fdaResponse.results().stream()
                .map(item -> new DrugRecordApplicationDto(item.applicationNumber(), item.sponsorName()))
                .toList();
    }
}