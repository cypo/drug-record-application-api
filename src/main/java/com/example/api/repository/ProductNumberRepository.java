package com.example.api.repository;

import com.example.api.model.entity.ProductNumberEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductNumberRepository extends JpaRepository<ProductNumberEntity, Long> {
}