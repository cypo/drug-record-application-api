package com.example.api.repository;

import com.example.api.model.entity.DrugDetailsEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DrugApplicationDetailsRepository extends JpaRepository<DrugDetailsEntity, String> {
}