package com.example.api.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class DrugRecordRequest extends Paging {

    private String fdaManufacturerName;
    private String optionalFdaBrandName;
}