package com.example.api.model;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Optional;

@EqualsAndHashCode
@NoArgsConstructor
public class Paging {

    private Integer pageSize;
    private Integer pageNumber;

    public int getPageSize() {
        return Optional.ofNullable(pageSize)
                .orElse(10);
    }

    public int getPageNumber() {
        return Optional.ofNullable(pageNumber)
                .orElse(1);
    }
}