package com.example.api.model.fda;

import java.util.List;

public record FdaResponse(Meta meta, List<FdaResult> results) {
}
