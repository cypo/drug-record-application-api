package com.example.api.model.fda;

import com.fasterxml.jackson.annotation.JsonProperty;


public record FdaResult(
        @JsonProperty("application_number") String applicationNumber,
        @JsonProperty("sponsor_name") String sponsorName
        //rest of fields if needed
) {

}