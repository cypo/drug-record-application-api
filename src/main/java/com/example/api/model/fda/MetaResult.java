package com.example.api.model.fda;

public record MetaResult(long skip, long limit, long total) {
}
