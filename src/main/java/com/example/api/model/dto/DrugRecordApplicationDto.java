package com.example.api.model.dto;

public record DrugRecordApplicationDto(String applicationNumber, String sponsorName) {
}
