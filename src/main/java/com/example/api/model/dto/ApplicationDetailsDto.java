package com.example.api.model.dto;

import java.util.List;

public record ApplicationDetailsDto(
        String number,
        String manufacturerName,
        String substanceName,
        List<String> productNumbers) {
}