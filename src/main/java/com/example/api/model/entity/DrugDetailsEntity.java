package com.example.api.model.entity;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Entity
@Table(name = "application_details")
@NoArgsConstructor
public class DrugDetailsEntity {
    private @Id String number;
    private String manufacturerName;
    private String substanceName;
    @OneToMany(mappedBy = "details")
    private List<ProductNumberEntity> productNumbers;

    public DrugDetailsEntity(String number, String manufacturerName, String substanceName) {
        this.number = number;
        this.manufacturerName = manufacturerName;
        this.substanceName = substanceName;
    }
}