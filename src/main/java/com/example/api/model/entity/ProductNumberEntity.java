package com.example.api.model.entity;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@Table(name = "product_numbers")
public class ProductNumberEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String number;
    @ManyToOne
    @JoinColumn(name="details_id")
    private DrugDetailsEntity details;


    public ProductNumberEntity(String number, DrugDetailsEntity details) {
        this.number = number;
        this.details = details;
    }
}