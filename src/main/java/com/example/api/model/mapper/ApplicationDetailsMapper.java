package com.example.api.model.mapper;

import com.example.api.model.dto.ApplicationDetailsDto;
import com.example.api.model.entity.DrugDetailsEntity;
import com.example.api.model.entity.ProductNumberEntity;

public class ApplicationDetailsMapper {

    private ApplicationDetailsMapper() {
    }

    public static ApplicationDetailsDto toDto(DrugDetailsEntity entity) {
        return new ApplicationDetailsDto(entity.getNumber(),
                entity.getManufacturerName(),
                entity.getSubstanceName(),
                entity.getProductNumbers().stream()
                        .map(ProductNumberEntity::getNumber).toList());
    }

    public static DrugDetailsEntity toEntity(ApplicationDetailsDto dto) {
        return new DrugDetailsEntity(dto.number(), dto.manufacturerName(), dto.substanceName());
    }
}
