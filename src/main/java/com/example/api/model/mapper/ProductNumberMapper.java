package com.example.api.model.mapper;

import com.example.api.model.dto.ApplicationDetailsDto;
import com.example.api.model.entity.DrugDetailsEntity;
import com.example.api.model.entity.ProductNumberEntity;

import java.util.List;

public class ProductNumberMapper {

    private ProductNumberMapper() {
    }

    public static List<ProductNumberEntity> toEntity(ApplicationDetailsDto dto, DrugDetailsEntity entity) {
        return dto.productNumbers().stream()
                .map(number -> new ProductNumberEntity(number, entity)).toList();
    }
}
