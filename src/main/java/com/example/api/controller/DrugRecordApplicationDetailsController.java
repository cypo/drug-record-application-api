package com.example.api.controller;

import com.example.api.model.dto.ApplicationDetailsDto;
import com.example.api.service.DrugRecordApplicationDetails;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.List;

@RestController
@RequestMapping("/v1/drug-record-application/details")
@RequiredArgsConstructor
public class DrugRecordApplicationDetailsController {

    private final DrugRecordApplicationDetails drugRecordApplicationDetails;

    @PostMapping
    public ResponseEntity<?> saveDetails(@RequestBody ApplicationDetailsDto dto) {
        String id = drugRecordApplicationDetails.save(dto);
        return ResponseEntity.created(ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(id)
                .toUri()).build();
    }

    @GetMapping
    public ResponseEntity<List<ApplicationDetailsDto>> getDetails() {
        return ResponseEntity.ok(drugRecordApplicationDetails.read());
    }
}