package com.example.api.controller;

import com.example.api.model.dto.DrugRecordApplicationDto;
import com.example.api.model.DrugRecordRequest;
import com.example.api.service.DrugRecordApplications;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/drug-record-application")
@RequiredArgsConstructor
public class DrugRecordApplicationSearchController {

    private final DrugRecordApplications drugRecordApplications;

    @GetMapping("/registered")
    public ResponseEntity<Page<DrugRecordApplicationDto>> getDrugRecordApplication(DrugRecordRequest request) {
        return ResponseEntity.ok(drugRecordApplications.getPage(request));
    }
}