package com.example.api.controller;

import com.example.api.exception.NoResultsFoundException;
import com.example.api.model.dto.DrugRecordApplicationDto;
import com.example.api.service.DrugRecordApplications;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;

import java.util.List;

import static io.restassured.RestAssured.given;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class DrugRecordApplicationSearchControllerTest {

    private static final String ENDPOINT_PATH = "/v1/drug-record-application/registered";

    @LocalServerPort
    private int port;
    @MockBean
    private DrugRecordApplications drugRecordApplications;

    @Test
    void shouldReturnHttp200WhenSomeResultsFound() {
        List<DrugRecordApplicationDto> result = List
                .of(new DrugRecordApplicationDto("number", "name"));
        Mockito.when(drugRecordApplications.getPage(any()))
                .thenReturn(new PageImpl<>(result, PageRequest.of(1, 1), 1));

        given()
                .port(port)
                .when()
                .get(ENDPOINT_PATH)
                .then()
                .statusCode(HttpStatus.OK.value());
    }

    @Test
    void shouldReturnHttp204OnNoResultsFoundException() {
        Mockito.when(drugRecordApplications.getPage(any()))
                        .thenThrow(new NoResultsFoundException());

        given()
                .port(port)
                .when()
                .get(ENDPOINT_PATH)
                .then()
                .statusCode(HttpStatus.NO_CONTENT.value());
    }
}