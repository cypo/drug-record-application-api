package com.example.api.controller;

import com.example.api.model.dto.ApplicationDetailsDto;
import io.restassured.http.ContentType;
import org.apache.http.HttpHeaders;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;

import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(MockitoExtension.class)
class DrugRecordApplicationDetailsControllerTest {

    private static final String ENDPOINT_PATH = "/v1/drug-record-application/details";

    @LocalServerPort
    private int port;

    @Test
    void shouldReturnHttp201AndLocation() {
        given()
                .port(port)
                .contentType(ContentType.JSON)
                .body(new ApplicationDetailsDto(
                        "123",
                        "name",
                        "substanceName",
                        List.of("ABC1")))
                .when()
                .post(ENDPOINT_PATH)
                .then()
                .statusCode(HttpStatus.CREATED.value())
                .header(HttpHeaders.LOCATION, containsString(ENDPOINT_PATH + "/123"));
    }

    @Test
    void shouldReturnHttp200() {
        given()
                .port(port)
                .when()
                .get(ENDPOINT_PATH)
                .then()
                .statusCode(HttpStatus.OK.value());
    }
}