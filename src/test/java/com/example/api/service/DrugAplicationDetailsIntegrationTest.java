package com.example.api.service;

import com.example.api.model.dto.ApplicationDetailsDto;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class DrugAplicationDetailsIntegrationTest {

    @Autowired
    private DrugRecordApplicationDetails service;

    @Test
    void shouldSaveAndReturnDetails() {
        //GIVEN
        ApplicationDetailsDto inputDto = new ApplicationDetailsDto(
                "ABC100",
                "name",
                "substance",
                List.of("number1", "number2"));

        //WHEN
        service.save(inputDto);

        //AND
        List<ApplicationDetailsDto> actual = service.read();

        //THEN
        Assertions.assertThat(actual).isEqualTo(List.of(inputDto));
    }
}
