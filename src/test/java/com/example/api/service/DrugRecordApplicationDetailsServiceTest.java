package com.example.api.service;

import com.example.api.model.dto.ApplicationDetailsDto;
import com.example.api.model.entity.DrugDetailsEntity;
import com.example.api.model.entity.ProductNumberEntity;
import com.example.api.repository.DrugApplicationDetailsRepository;
import com.example.api.repository.ProductNumberRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
class DrugRecordApplicationDetailsServiceTest {

    @Spy
    private DrugApplicationDetailsRepository repository;
    @Spy
    private ProductNumberRepository productNumberRepository;

    @Test
    void shouldCallRepositories() {
        //GIVEN
        DrugDetailsEntity drugDetailsEntity = new DrugDetailsEntity("100", "name", "substance");
        ProductNumberEntity productNumberEntity = new ProductNumberEntity("123", drugDetailsEntity);
        Mockito.when(repository.save(any()))
                .thenReturn(drugDetailsEntity);

        //WHEN
        new DrugRecordApplicationDetailsService(repository, productNumberRepository)
                .save(new ApplicationDetailsDto("100", "name", "substance", List.of("123")));

        //THEN
        Mockito.verify(repository, Mockito.times(1)).save(drugDetailsEntity);
        Mockito.verify(productNumberRepository, Mockito.times(1)).saveAll(List.of(productNumberEntity));
    }

    @Test
    void shouldReturnId() {
        //GIVEN
        Mockito.when(repository.save(any()))
                .thenReturn(new DrugDetailsEntity("100", "", ""));

        //WHEN
        String id = new DrugRecordApplicationDetailsService(repository, productNumberRepository)
                .save(new ApplicationDetailsDto("100", "", "", Collections.emptyList()));

        //THEN
        Assertions.assertThat(id).isEqualTo("100");
    }

    @Test
    void shouldReturnMappedDataFromRepository() {
        //GIVEN
        DrugDetailsEntity entity = new DrugDetailsEntity("99", "name", "substance");
        entity.setProductNumbers(List.of(new ProductNumberEntity("1", entity)));
        Mockito.when(repository.findAll())
                .thenReturn(List.of(entity));

        //WHEN
        List<ApplicationDetailsDto> actual = new DrugRecordApplicationDetailsService(repository, productNumberRepository).read();


        //THEN
        List<ApplicationDetailsDto> expected = List.of(new ApplicationDetailsDto(
                entity.getNumber(),
                entity.getManufacturerName(),
                entity.getSubstanceName(),
                List.of("1")));

        Assertions.assertThat(actual).isEqualTo(expected);
    }
}