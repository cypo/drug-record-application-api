package com.example.api.service;

import com.example.api.exception.NoResultsFoundException;
import com.example.api.model.dto.DrugRecordApplicationDto;
import com.example.api.model.DrugRecordRequest;
import com.example.api.model.fda.FdaResponse;
import com.example.api.model.fda.FdaResult;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import java.io.IOException;
import java.util.List;

class FdaDrugRecordApplicationServiceTest {
    private static final String LOCALHOST_URL_TEMPLATE = "http://localhost:%s";
    private static MockWebServer mockWebServer;

    @BeforeAll
    static void setUp() throws IOException {
        mockWebServer = new MockWebServer();
        mockWebServer.start();
    }

    @AfterAll
    static void tearDown() throws IOException {
        mockWebServer.shutdown();
    }


    @Test
    void shouldReturnResultMappedToDto() throws JsonProcessingException {
        //GIVEN
        FdaResponse fdaMockResponse = new FdaResponse(null, List.of(
                new FdaResult("111", "name1"),
                new FdaResult("222", "name2")));

        mockWebServer.enqueue(new MockResponse()
                .setBody(new ObjectMapper().writeValueAsString(fdaMockResponse))
                .setHeader("content-type", MediaType.APPLICATION_JSON_VALUE));

        //WHEN
        Page<DrugRecordApplicationDto> result = new FdaDrugRecordApplicationService(getMockedServerHost())
                .getPage(new DrugRecordRequest());

        //THEN
        List<FdaResult> mockedResults = fdaMockResponse.results();
        List<DrugRecordApplicationDto> expectedContent = List.of(
                new DrugRecordApplicationDto(mockedResults.get(0).applicationNumber(), mockedResults.get(0).sponsorName()),
                new DrugRecordApplicationDto(mockedResults.get(1).applicationNumber(), mockedResults.get(1).sponsorName()));
        Assertions.assertThat(result.getContent()).isEqualTo(expectedContent);
    }

    @Test
    void shouldThrowNoResultsFoundExceptionOn404() {
        //GIVEN
        mockWebServer.enqueue(new MockResponse().setResponseCode(HttpStatus.NOT_FOUND.value()));

        //WHEN/THEN
        Assertions.assertThatThrownBy(() -> new FdaDrugRecordApplicationService(getMockedServerHost())
                .getPage(new DrugRecordRequest())).isInstanceOf(NoResultsFoundException.class);
    }

    private static String getMockedServerHost() {
        return String.format(LOCALHOST_URL_TEMPLATE, mockWebServer.getPort());
    }
}