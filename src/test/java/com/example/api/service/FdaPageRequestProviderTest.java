package com.example.api.service;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

class FdaPageRequestProviderTest {

    @ParameterizedTest
    @MethodSource("pagingTestInput")
    void shouldReturnPagingRequest(int requestedPageSize, int requestedPageNumber, int expectedSkip) {
        //GIVEN
        //WHEN
        FdaPageRequestProvider result = new FdaPageRequestProvider(requestedPageSize, requestedPageNumber);

        //THEN
        Assertions.assertThat(result.getSkip()).isEqualTo(expectedSkip);
    }

    private static Stream<Arguments> pagingTestInput(){
        return Stream.of(
                Arguments.of(10, 1, 0),
                Arguments.of(10, 2, 10),
                Arguments.of(10, 3, 20));
    }
}