# Drug Record Application API

### Requirements
To launch this application you need to have:
- JDK 17
- maven
- free 8080 port

### Build
In order to build and run the application locally run the following commands:<BR>
mvn clean install<BR>
mvn spring-boot:run


### Test
To execute all unit and integration tests run the command:
mvn test

### Endpoints
The API documentation is available here: http://localhost:8080/swagger-ui.html (application needs to be running)

### Data storage
Application uses in-memory database (H2) with PostgreSQL dialect.
It can be easily changed to a file-system storage, by modifying value in application.properties file:<BR>
spring.datasource.url=jdbc:h2:<path_to_storage_file>
